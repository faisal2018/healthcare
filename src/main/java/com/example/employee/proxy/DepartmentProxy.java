package com.example.employee.proxy;

import com.example.employee.entity.Department;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "department-service")
public interface DepartmentProxy {
    @GetMapping("/department/{id}")
    Department fetchDepartment(@PathVariable Long id);
}
