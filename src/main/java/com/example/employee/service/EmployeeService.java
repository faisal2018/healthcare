package com.example.employee.service;

import com.example.employee.config.RestClientConfig;
import com.example.employee.dto.EmployeeDto;
import com.example.employee.dto.EmployeeResponseDto;
import com.example.employee.entity.Department;
import com.example.employee.entity.Employee;
import com.example.employee.proxy.DepartmentProxy;
import com.example.employee.repo.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final DepartmentProxy departmentProxy;
    private final RestClientConfig restClientConfig;
    private final RestTemplate restTemplate;

    public List<Employee> fetchAllEmployee() {
        return employeeRepository.findAll();
    }

    public EmployeeDto saveEmployee(EmployeeDto employeeDto) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeDto, employee);
        employee = employeeRepository.save(employee);
        BeanUtils.copyProperties(employee, employeeDto);
        return employeeDto;
    }

    public EmployeeResponseDto fetchEmployee(Long deptId) {
        EmployeeResponseDto employeeResponseDto = new EmployeeResponseDto();
        List<Employee> empList = employeeRepository.findByDeptId(deptId);
        employeeResponseDto.setEmployee(empList);
        Department department = departmentProxy.fetchDepartment(deptId);
        employeeResponseDto.setDepartment(department);//http://localhost:9089/department/1
        /*without load balancing*/
        //Department departmentRest = restTemplate.getForObject("http://localhost:9089/department/"+deptId,Department.class);
        /*with load balancing*/
        Department departmentRest = restTemplate.getForObject("http://department-service/department/"+deptId,Department.class);
        System.out.println(departmentRest);
        return employeeResponseDto;
    }


}
