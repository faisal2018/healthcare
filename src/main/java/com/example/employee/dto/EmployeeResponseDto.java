package com.example.employee.dto;

import com.example.employee.entity.Department;
import com.example.employee.entity.Employee;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class EmployeeResponseDto {
    List<Employee> employee;
    Department department;
}
