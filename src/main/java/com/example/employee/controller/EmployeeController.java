package com.example.employee.controller;

import com.example.employee.dto.EmployeeDto;
import com.example.employee.dto.EmployeeResponseDto;
import com.example.employee.entity.Employee;
import com.example.employee.service.EmployeeService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "EmployeeController", description = "REST Apis related to Employee!!!!")
@RestController
@RequiredArgsConstructor
public class EmployeeController {

/*    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success|OK"),
            @ApiResponse(code = 401, message = "not authorized!"),
            @ApiResponse(code = 403, message = "forbidden!!!"),
            @ApiResponse(code = 404, message = "not found!!!")})*/

    private final EmployeeService employeeService;

    @GetMapping("/getAllEmployee")
    public List<Employee> getAllDepartment() {
        return employeeService.fetchAllEmployee();
    }

    @PostMapping("/employee")
    public EmployeeDto saveDepartment(@RequestBody EmployeeDto employeeDto) {
        return employeeService.saveEmployee(employeeDto);
    }

    @GetMapping("/fetchEmployee/{deptId}")
    public EmployeeResponseDto fetchEmployee(@PathVariable Long deptId) {
        return employeeService.fetchEmployee(deptId);
    }
}
